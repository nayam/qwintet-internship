import java.time.LocalTime

fun main(args: Array<String>) {
    database.forEach { mine ->
        database.forEach { yours ->
            println(mine.similarity(yours))
        }
        println()
    }
}

private val database by lazy {
    listOf(
            Employee(33, Department.DEVELOPMENT, Genre.JAPANESE, LocalTime.of(12, 30), setOf(Hobby.READING), 40, setOf(Team.A)),
            Employee(27, Department.DESIGNING, Genre.WESTERN, LocalTime.of(13, 0), setOf(Hobby.WATCHING_TV, Hobby.FAMILY_TIME), 30, setOf(Team.A)),
            Employee(21, Department.DEVELOPMENT, Genre.JAPANESE, LocalTime.of(12, 0), setOf(Hobby.WATCHING_TV, Hobby.READING), 60, setOf(Team.B)),
            Employee(30, Department.PLANNING, Genre.ITALIAN, LocalTime.of(14, 0), setOf(Hobby.READING), 20, setOf(Team.A)),
            Employee(25, Department.PLANNING, Genre.ITALIAN, LocalTime.of(12, 0), setOf(Hobby.WATCHING_TV), 40, setOf(Team.B)))
}

private val ageStandardDeviation by lazy {
    database.map(Employee::age).max()!! - database.map(Employee::age).min()!!
}

private fun LocalTime.toMinute() = hour * 60 + minute

private val beginningTimeStandardDeviation by lazy {
    database.map(Employee::beginningTime).max()!!.toMinute() - database.map(Employee::beginningTime).min()!!.toMinute()
}
private val requiredMinuteStandardDeviation by lazy {
    database.map(Employee::timeRequiredMinute).max()!! - database.map(Employee::timeRequiredMinute).min()!!
}

fun Iterable<Double>.standardDeviation(): Double {
    val mean = average()
    val variance = map { Math.pow(it - mean, 2.0) }.average()
    return Math.sqrt(variance)
}

private fun <T> Iterable<T>.diceCoefficient(other: Iterable<T>) = intersect(other).size * 2.0 / (count() + other.count())

private fun Int.normalizedEuclideanDistance(other: Int, standardDeviation: Double) = 1 - Math.abs(this - other) / standardDeviation
private fun LocalTime.normalizedEuclideanDistance(other: LocalTime, standardDeviation: Double) = 1 - (Math.abs((hour - other.hour) * 60 + minute - other.minute) / standardDeviation)

enum class Department(private val string: String) {
    DEVELOPMENT("開発"), DESIGNING("デザイン"), PLANNING("企画");

    override fun toString() = string
}

enum class Genre(private val string: String) {
    JAPANESE("和"), WESTERN("洋"), ITALIAN("イタリア");

    override fun toString() = string
}

enum class Hobby(private val string: String) {
    READING("読書"), WATCHING_TV("テレビ"), FAMILY_TIME("家族と過ごす");

    override fun toString() = string
}

enum class Team(private val string: String) {
    A("A"), B("B"), C("C");

    override fun toString() = string
}

private val weightAge by lazy { 1.0 }
private val weightDepartment by lazy { 1.0 }
private val weightGenre by lazy { 1.0 }
private val weightBeginningTime by lazy { 1.0 }
private val weightHobbies by lazy { 1.0 }
private val weightTimeRequiredMinute by lazy { 1.0 }
private val weightTeam by lazy { 1.0 }

data class Employee(
        val age: Int,
        val department: Department,
        val genre: Genre,
        val beginningTime: LocalTime,
        val hobbies: Set<Hobby>,
        val timeRequiredMinute: Int,
        val team: Set<Team>) {

    // AC 高そう
    //
    // AD 40
    // DE 高杉

    fun similarity(other: Employee) =
            (setOf(genre).diceCoefficient(setOf(other.genre)) * weightGenre +
                    beginningTime.normalizedEuclideanDistance(other.beginningTime, beginningTimeStandardDeviation.toDouble()) * weightBeginningTime +
                    hobbies.diceCoefficient(other.hobbies) * weightHobbies +
                    timeRequiredMinute.normalizedEuclideanDistance(other.timeRequiredMinute, requiredMinuteStandardDeviation.toDouble()) * weightTimeRequiredMinute) /
                    (weightGenre + weightBeginningTime + weightHobbies + weightTimeRequiredMinute)


    fun dissimilarity(other: Employee) =
            ((1- age.normalizedEuclideanDistance(other.age, ageStandardDeviation.toDouble())) * weightAge +
                    (1- setOf(department).diceCoefficient(setOf(other.department))) * weightDepartment +
                    (1- team.diceCoefficient(other.team)) * weightTeam) /
                    (weightAge + weightDepartment + weightTeam)


}